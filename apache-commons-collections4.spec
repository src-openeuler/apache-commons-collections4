Summary:             Extension of the Java Collections Framework
Name:                apache-commons-collections4
Version:             4.4
Release:             1
License:             ASL 2.0
URL:                 http://commons.apache.org/proper/commons-collections/
Source0:             http://archive.apache.org/dist/commons/collections/source/commons-collections4-%{version}-src.tar.gz
BuildRequires:       maven-local mvn(junit:junit) mvn(org.apache.commons:commons-parent:pom:)
BuildRequires:       mvn(org.apache.maven.plugins:maven-antrun-plugin) mvn(org.easymock:easymock)
BuildRequires:       mvn(org.apache.commons:commons-lang3)
BuildArch:           noarch

%description
Commons-Collections seek to build upon the JDK classes by providing
new interfaces, implementations and utilities.

%package javadoc
Summary:             API documentation for %{name}
%description javadoc
This package provides %{summary}.

%prep
%setup -q -n commons-collections4-%{version}-src
%mvn_file : commons-collections4 %{name}

%build
%mvn_build -- -Dcommons.osgi.symbolicName=org.apache.commons.collections4

%install
%mvn_install

%files -f .mfiles
%doc RELEASE-NOTES.txt
%license LICENSE.txt NOTICE.txt

%files javadoc -f .mfiles-javadoc
%license LICENSE.txt NOTICE.txt

%changelog
* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 4.4-1
- Upgrade to version 4.4

* Fri Jul 09 2021 wangyue <wangyue92@huawei.com> - 4.1-2
- Add mvn(org.apache.commons:commons-lang3) buildrequire to fix build error

* Mon Jul 27 2020 chengzihan <chengzihan2@huawei.com> - 4.1-1
- Package init
